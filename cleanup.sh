#!/usr/bin/env sh

set -e
cd "$(dirname "${0}")"

. ./include.sh

exec ionice -c idle nice -n 19 restic forget --keep-daily 7 --keep-weekly 5 --keep-monthly 12 --keep-yearly 75 --keep-tag=keep --prune
# restic check
