# Restic backup scripts

Scripts and files (config) for backing up `$HOME` with `systemd --user`.

## Install

- Copy `./include_example.sh` to `./include.sh` and insert secrets there.
- Add patterns to ignore into `./exclude.lst`.
- Symlink all files from `./systemd/` to `~/.config/systemd/user/`
- Enable all timer
- Profit

## License

(c) 2018 sedrubal - [MIT](https://opensource.org/licenses/MIT)
