#!/usr/bin/env bash

RESTIC_PID=$(pidof restic)

set -e
cd "$(dirname "${0}")"

. ./include.sh

if [[ -z ${RESTIC_PID} ]]; then
    echo "[i] Restic is not running. Removing locks..."
    restic unlock
else
    echo "[!] Restic is still running. Skipping backup..."
    exit 0
fi

ionice -c idle nice -n 19 restic backup --one-file-system --exclude-file=./exclude.lst --exclude-if-present=.nobackup --exclude-caches "${HOME}"
exec ./cleanup.sh
# restic check
